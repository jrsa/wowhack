#include <iostream>

#include <thread>
#include <chrono>

#include <cstdlib> // atoi

#include <mach/mach.h>
#include <mach/mach_vm.h>

#include "memory.hpp"
#include "wow_structs_5875.h"

#define ADDR_NOT_FOUND  -1

using namespace std;

#pragma mark CGCamera


uint32_t getActiveCameraBaseAddress(mach_port_t task) {
    uint32_t pWorldFrame = read<uint32_t>(task, s_currentWorldFrame, 1);

    if (!pWorldFrame) {
        return 0;
    }

    return read<uint32_t>(task, pWorldFrame + worldFrame_cameraOffset, 1);
}

float getCameraFov(mach_port_t task, uint32_t cameraBaseAddress) {
    return read<float>(task, cameraBaseAddress + CGCamera__Fov_offset, 1);
}

void setCameraFov(mach_port_t task, uint32_t cameraBaseAddress, float newValue) {
    write(task, cameraBaseAddress + CGCamera__Fov_offset, newValue);
}

#pragma mark CM2Model

void setModelEmissive(mach_port_t task, uint32_t modelBaseAddress, float r, float g, float b) {
    write(task, modelBaseAddress + CM2Model_emissiveColor, r);
    write(task, modelBaseAddress + CM2Model_emissiveColor + 4, g);
    write(task, modelBaseAddress + CM2Model_emissiveColor + 8, b);
}

#pragma mark CGGameObject_C

std::string getGameObjectName(mach_port_t task, uint32_t gameObjectBase) {
    uint32_t stats = read<uint32_t>(task, gameObjectBase + CGGameObject_C__m_stats, 1);
    if (stats) {
        uint32_t pName = read<uint32_t>(task, stats + GameObjectStats__m_name, 1);
        if (pName) {
            const char* name = readToNewBuffer<char>(task, pName, 0x20);
            string result (name);
            delete name;
            return result;
        }
    }
    return "Unknown GameObject";
}

uint32_t getGameObjectType(mach_port_t task, uint32_t gameObjectBase) {
    uint32_t pData = read<uint32_t>(task, gameObjectBase + CGGameObject_C__m_data, 1);
    if (pData) {
        return read<uint32_t>(task, pData + CGGameObjectData__type, 1);
    } else {
        return -1;
    }
}

#pragma mark CGObject_C

uint32_t getObjectModel(mach_port_t task, uint32_t objectBaseAddress) {
    uint32_t model = read<uint32_t>(task, objectBaseAddress + CGObject_C__otherModel, 1);

    if (!model) {
        model = read<uint32_t>(task, objectBaseAddress + CGObject_C__characterModel, 1);
    }

    if (!model) {
        return ADDR_NOT_FOUND;
    } else {
        return model;
    }
}
#pragma mark CGUnit_C

uint32_t getUnitCurrentHp(mach_port_t task, uint32_t unitBaseAddress) {
    uint32_t pData = read<uint32_t>(task, unitBaseAddress + CGUnit_C__m_data, 1);

    if (pData) {
        return read<uint32_t>(task, pData + CGUnitData__curHealth, 1);
    }

    return ADDR_NOT_FOUND;
}

void printUnitInfo(mach_port_t task, uint32_t unitBaseAddress) {
    uint32_t pData = read<uint32_t>(task, unitBaseAddress + CGUnit_C__m_data, 1);

    if (pData) {
        uint32_t hp = read<uint32_t>(task, pData + CGUnitData__curHealth, 1);
        uint32_t maxHp = read<uint32_t>(task, pData + CGUnitData__maxHealth, 1);

        cout << hp << "/" << maxHp << endl;
    }

    uint32_t pMovementData = read<uint32_t>(task, unitBaseAddress + CGUnit_C__movementData2, 1);
    if (pMovementData) {
        float posx = read<float>(task, pMovementData + CMovementData_position, 1);
        float posy = read<float>(task, pMovementData + CMovementData_position + 4, 1);
        float posz = read<float>(task, pMovementData + CMovementData_position + 8, 1);

        float facing = read<float>(task, pMovementData + CMovementData_facing, 1);

        uint16_t movementType = read<uint16_t>(task, pMovementData + CMovementData_movementType, 1);

        cout << "position: " << posx << ", " << posy << ", " << posz << endl;
        cout << "facing: " << facing << endl;
        cout << "movement type: " << movementType << endl;
    }
}

void getUnitPosition(mach_port_t task, uint32_t unitBaseAddress, C3Vector& outPosition) {
    uint32_t pMovementData = read<uint32_t>(task, unitBaseAddress + CGUnit_C__movementData2, 1);

    if (pMovementData) {
        outPosition = read<C3Vector>(task, pMovementData + CMovementData_position, 1);
    }
}

string getUnitName(mach_port_t task, uint32_t unitBaseAddress) {
    uint32_t pStats = read<uint32_t>(task, unitBaseAddress + CGUnit_C__m_stats, 1);
    if (pStats) {
        uint32_t pName = read<uint32_t>(task, pStats, 1);
        char* name = readToNewBuffer<char>(task, pName, 0x20);
        string result (name);
        delete name;
        return result;
    } else {
        return "Unknown Being";
    }
}

int main(int argc, const char * argv[]) {

    kern_return_t code;
    mach_port_t task;
    int pid = 0;

    if (argc <= 1) {
        cout << "enter pid: "; cin >> pid; cout << endl;
    } else {
        pid = std::atoi(argv[1]);
    }

    code = task_for_pid(mach_task_self(), pid, &task);

    if ((code != KERN_SUCCESS)) {
        cerr << "task_for_pid() failed, (" << code << "): " << mach_error_string(code) << endl;
        return -1;
    }

    // enables interesting debug messages
    write(task, (vm_address_t) OUTPUT_DEBUG_STRING, 1);

    uint32_t nGameObjects = 0;
    uint32_t nUnits= 0;
    uint32_t nBadDataPtrs = 0;

    while (1) {
        // get the address of the object manager
        uint32_t objectManagerInstance = read<uint32_t>(task, (vm_address_t) s_curMgr, 1);

        if (!objectManagerInstance) {
            // probably in login screen, not ingame
            cout << "not ingame, skipping obj mgr traversal" << endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            continue;
        }

        // get three important fields from the object manager
        int64_t playerGuid = read<int64_t>(task, objectManagerInstance + s_curMgr__PLAYERGUID, 1);
        uint32_t first = read<uint32_t>(task, objectManagerInstance + s_curMgr__FIRSTOBJ, 1);
        uint32_t next = read<uint32_t>(task, objectManagerInstance + s_curMgr__NEXTOBJ, 1);

        int64_t currentGuid = -1;

        nGameObjects = 0;  nUnits = 0; nBadDataPtrs = 0;

        int i = 0;
        while(next && ((next & 1) == 0))
        {
            next = read<uint32_t>(task, first + next + 4, 1);
            currentGuid = read<int64_t>(task, next + CGObject_C__GUID, 1);

            // what type of object?
            uint32_t m_data = read<uint32_t>(task, next + CGObject_C__DATA, 1);
            uint32_t type = 0;

            try {
                if (m_data) {
                    type = read<uint32_t>(task, m_data + CGObjectData__TYPE, 1);
                }
            } catch (std::runtime_error e) {
            }

            if (currentGuid == playerGuid) {
                uint32_t model = getObjectModel(task, next);
                if (model != ADDR_NOT_FOUND) {
                    setModelEmissive(task, model, 0.0, 1.0, 0.0);
                }
            }

            else if (type & UNIT) {
                uint32_t model = getObjectModel(task, next);
                if (model != ADDR_NOT_FOUND) {
                    setModelEmissive(task, model, 0.0, 0.0, 1.0);
                }
            }
            

            if (type & GAMEOBJECT) {
                uint32_t model = getObjectModel(task, next);
                if (model != ADDR_NOT_FOUND) {
                    setModelEmissive(task, model, 1.0, 0.0, 0.0);
                }
            }
        }
    }
    return 0;
}
