#ifndef wow_structs_5875_h
#define wow_structs_5875_h

#include <stdint.h>

// pointers for client version 1.12.1 build 5875

// found inside ObjUsage console command
// 0x00060401 mov ecx, dword [ds:0x99066c]; 0x99066c is the static objMgr pointer
#define s_curMgr  0x99066c
#define s_curMgr__PLAYERGUID 0xCC
#define s_curMgr__FIRSTOBJ   0xAC
#define s_curMgr__NEXTOBJ    0xB4

#define CGObject_C__GUID    0x28
#define CGObject_C__DATA    0x4
#define CGObject_C__characterModel  0xD0
#define CGObject_C__otherModel  0xD4

#define CGObjectData__TYPE  0x8

#define CGUnit_C__m_stats   0xb24

#define CGUnit_C__m_data        0x108
#define CGUnit_C__s_activeMover 0x699260
#define CGUnit_C__movementData  0x9A0
#define CGUnit_C__movementData2 0x110

#define CMovementData_position   0x10
#define CMovementData_facing     0x1C
#define CMovementData_movementType  0x3E

#define CGUnitData__SIZE        0x2d4
#define CGUnitData__maxHealth   0x58
#define CGUnitData__curHealth   0x40

#define CGGameObject_C__m_stats 524
#define CGGameObject_C__m_data  0x108
#define GameObjectStats__m_name 8
#define CGGameObjectData__type  0xc

#define CM2Model_emissiveColor  0x18C
#define CM2Model_data1          0x30

#define s_localPlayerUpdates    0x0990694

#define s_currentWorldFrame 0xb21be8
#define worldFrame_cameraOffset 0x65cc
#define CGCamera__Fov_offset 0x40
#define CGCamera__nearClip_offset 0x38
#define CGCamera__farClip 0x3c
#define CGCamera__Flags_offset 0x8C

// 0x004cf904 db "1.12.1", 0; XREF=sub_d92fa+152, sub_15689e+10
#define s_version 0x004cf904
#define s_version_LEN 7

#define OUTPUT_DEBUG_STRING 0x0693660

enum OBJECT_TYPE {
    UNIT        = 8,
    PLAYER      = 16,
    GAMEOBJECT  = 32,
};

enum GAMEOBJECT_TYPE {
    LOCKED      = 2,
    SOMETHING   = 32,
    TRANSPORT   = 64
};

struct C3Vector {
    float x;
    float y;
    float z;
};

#endif
