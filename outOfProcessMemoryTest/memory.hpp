#ifndef MEMORY
#define MEMORY

#include <mach/mach.h>
#include <mach/mach_vm.h>

#include <exception>

template <typename T>
T read(mach_port_t task, uint32_t addr, size_t elements) {

    T result;
    vm_size_t bytesRead = 0;

    uint32_t bytesToRead = sizeof(T) * (uint32_t)elements;

    kern_return_t error = vm_read_overwrite(task, (vm_address_t) addr, bytesToRead, (vm_address_t)&result, &bytesRead);

    if (error != KERN_SUCCESS) {
        throw std::runtime_error(mach_error_string(error));
    }

    return result;
}


template <typename T>
T* readToNewBuffer(mach_port_t task, uint32_t addr, size_t elements) {

    T* result = nullptr;
    vm_size_t bytesRead = 0;

    uint32_t bytesToRead = sizeof(T) * (uint32_t)elements;

    result = new T[elements];

    kern_return_t error = vm_read_overwrite(task, (vm_address_t) addr, bytesToRead, (vm_address_t)result, &bytesRead);

    if (error != KERN_SUCCESS) {
        throw std::runtime_error(mach_error_string(error));
    }

    return result;
}

template <typename T>
void write(mach_port_t task, vm_address_t addr, T element) {
    kern_return_t error = vm_write(task, addr, (vm_offset_t)&element, sizeof(T));

    if (error != KERN_SUCCESS) {
        throw std::runtime_error(mach_error_string(error));
    }
}


#endif
